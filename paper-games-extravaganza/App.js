// Core
import React from 'react';
import { Text, View } from 'react-native';

// Extras
import { StackNavigator, createStackNavigator } from "react-navigation";

// Screens
import HomeScreen from "./app/screens/home.js"
// import BattleShipScreen from "./app/screens/battleship.js";
// import BingoScreen from "./app/screens/bingo.js";
// import BullsAndCowsScreen from "./app/screens/bulls-and-cows.js";
// import ChompScreen from "./app/screens/chomp.js";
// import CountDownScreen from "./app/screens/countdown.js";
// import EyeTestScreen from "./app/screens/eye-test.js";
// import GuessMyNumberScreen from "./app/screens/guess-my-number.js";
// import HangmanScreen from "./app/screens/hangman.js";
// import MASHScreen from "./app/screens/mash.js";
// import TicTacToeScreen from "./app/screens/tic-tac-toe.js";

const RootStack = createStackNavigator(
  {
    Home: HomeScreen,
    // BattleShip: BattleShipScreen,
    // Bingo: BingoScreen,
    // BullsAndCows: BullsAndCowsScreen,
    // Chomp: ChompScreen,
    // CountDown: CountDownScreen,
    // EyeTest: EyeTestScreen,
    // GuessMyNumber: GuessMyNumberScreen,
    // Hangman: HangmanScreen,
    // MASH: MASHScreen,
    // TicTacToe: TicTacToeScreen,
  },
  {
    initialRouteName: "Home"
  }
);

export default class App extends React.Component {
  render() {
    return (
      <RootStack />
    );
  }
}
